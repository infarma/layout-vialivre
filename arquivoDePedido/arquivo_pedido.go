package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	Cabecalho Cabecalho `json:"Cabecalho"`
	Detalhe   []Detalhe `json:"Detalhe"`
	Rodape    Rodape    `json:"Rodape"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		var index int32
		if identificador == "01" {
			err = arquivo.Cabecalho.ComposeStruct(string(runes))
		} else if identificador == "02" {
			err = arquivo.Detalhe[index].ComposeStruct(string(runes))
			index++
		} else if identificador == "03" {
			err = arquivo.Rodape.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}
