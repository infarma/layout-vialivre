package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Cabecalho struct {
	CodigoRegistro             string `json:"CodigoRegistro"`
	IdentificacaoPedidoCliente string `json:"IdentificacaoPedidoCliente"`
	CodigoCliente              string `json:"CodigoCliente"`
	CnpjCliente                int64  `json:"CnpjCliente"`
	CnpjFornecedor             int64  `json:"CnpjFornecedor"`
	DataPedido                 int32  `json:"DataPedido"`
}

func (c *Cabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalho

	err = posicaoParaValor.ReturnByType(&c.CodigoRegistro, "CodigoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.IdentificacaoPedidoCliente, "IdentificacaoPedidoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoCliente, "CodigoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjCliente, "CnpjCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjFornecedor, "CnpjFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataPedido, "DataPedido")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"CodigoRegistro":             {0, 2, 0},
	"IdentificacaoPedidoCliente": {2, 14, 0},
	"CodigoCliente":              {14, 24, 0},
	"CnpjCliente":                {24, 38, 0},
	"CnpjFornecedor":             {38, 52, 0},
	"DataPedido":                 {52, 60, 0},
}
