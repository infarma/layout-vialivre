package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Cabecalho struct {
	CodigoRegistro                  string  `json:"CodigoRegistro"`
	IdentificacaoPedidoCliente      string  `json:"IdentificacaoPedidoCliente"`
	CnpjEmissorNotaFiscal           string  `json:"CnpjEmissorNotaFiscal"`
	NumeroNotaFiscal                int32   `json:"NumeroNotaFiscal"`
	SerieNotaFiscal                 string  `json:"SerieNotaFiscal"`
	DataEmissaoNotaFiscal           int32   `json:"DataEmissaoNotaFiscal"`
	CodigoFiscalOperacao            int32   `json:"CodigoFiscalOperacao"`
	CodigoValorFiscal               int32   `json:"CodigoValorFiscal"`
	ValorBrutoNotaFiscal            float64 `json:"ValorBrutoNotaFiscal"`
	ValorContabilNotaFiscal         float64 `json:"ValorContabilNotaFiscal"`
	ValorTributadoNotaFiscal        float64 `json:"ValorTributadoNotaFiscal"`
	ValorOutrasNotaFiscal           float64 `json:"ValorOutrasNotaFiscal"`
	BaseICMSRetido                  float64 `json:"BaseICMSRetido"`
	ValorICMSRetido                 float64 `json:"ValorICMSRetido"`
	BaseICMSEntrada                 float64 `json:"BaseICMSEntrada"`
	ValorICMSEntrada                float64 `json:"ValorICMSEntrada"`
	BaseIPI                         float64 `json:"BaseIPI"`
	ValorIPI                        float64 `json:"ValorIPI"`
	CGCTransportadora               string  `json:"CGCTransportadora"`
	TipoFrete                       string  `json:"TipoFrete"`
	ValorFrete                      float64 `json:"ValorFrete"`
	PercentualDescontoComercial     float32 `json:"PercentualDescontoComercial"`
	PercentualDescontoRepasseICMS   float32 `json:"PercentualDescontoRepasseICMS"`
	TotalItens                      int64   `json:"TotalItens"`
	TotalUnidades                   float64 `json:"TotalUnidades"`
	TotalNotasParaPedido            int32   `json:"TotalNotasParaPedido"`
	ValorDescontoICMSNormal         float64 `json:"ValorDescontoICMSNormal"`
	ValorDescontoPIS                float64 `json:"ValorDescontoPIS"`
	ValorDescontoCofins             float64 `json:"ValorDescontoCofins"`
	TipoNotaFiscalEntrada           int32   `json:"TipoNotaFiscalEntrada"`
	ChaveAcessoNotaFiscalEletronica string  `json:"ChaveAcessoNotaFiscalEletronica"`
}

func (c *Cabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalho

	err = posicaoParaValor.ReturnByType(&c.CodigoRegistro, "CodigoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.IdentificacaoPedidoCliente, "IdentificacaoPedidoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjEmissorNotaFiscal, "CnpjEmissorNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroNotaFiscal, "NumeroNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.SerieNotaFiscal, "SerieNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataEmissaoNotaFiscal, "DataEmissaoNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoFiscalOperacao, "CodigoFiscalOperacao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoValorFiscal, "CodigoValorFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ValorBrutoNotaFiscal, "ValorBrutoNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ValorContabilNotaFiscal, "ValorContabilNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ValorTributadoNotaFiscal, "ValorTributadoNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ValorOutrasNotaFiscal, "ValorOutrasNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.BaseICMSRetido, "BaseICMSRetido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ValorICMSRetido, "ValorICMSRetido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.BaseICMSEntrada, "BaseICMSEntrada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ValorICMSEntrada, "ValorICMSEntrada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.BaseIPI, "BaseIPI")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ValorIPI, "ValorIPI")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CGCTransportadora, "CGCTransportadora")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoFrete, "TipoFrete")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ValorFrete, "ValorFrete")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PercentualDescontoComercial, "PercentualDescontoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PercentualDescontoRepasseICMS, "PercentualDescontoRepasseICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TotalItens, "TotalItens")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TotalUnidades, "TotalUnidades")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TotalNotasParaPedido, "TotalNotasParaPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ValorDescontoICMSNormal, "ValorDescontoICMSNormal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ValorDescontoPIS, "ValorDescontoPIS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ValorDescontoCofins, "ValorDescontoCofins")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoNotaFiscalEntrada, "TipoNotaFiscalEntrada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ChaveAcessoNotaFiscalEletronica, "ChaveAcessoNotaFiscalEletronica")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"CodigoRegistro":                  {0, 2, 0},
	"IdentificacaoPedidoCliente":      {2, 14, 0},
	"CnpjEmissorNotaFiscal":           {14, 28, 0},
	"NumeroNotaFiscal":                {28, 34, 0},
	"SerieNotaFiscal":                 {34, 37, 0},
	"DataEmissaoNotaFiscal":           {37, 45, 0},
	"CodigoFiscalOperacao":            {45, 49, 0},
	"CodigoValorFiscal":               {49, 50, 0},
	"ValorBrutoNotaFiscal":            {50, 65, 2},
	"ValorContabilNotaFiscal":         {65, 80, 2},
	"ValorTributadoNotaFiscal":        {80, 95, 2},
	"ValorOutrasNotaFiscal":           {95, 110, 2},
	"BaseICMSRetido":                  {110, 125, 2},
	"ValorICMSRetido":                 {125, 140, 2},
	"BaseICMSEntrada":                 {140, 155, 2},
	"ValorICMSEntrada":                {155, 170, 2},
	"BaseIPI":                         {170, 185, 2},
	"ValorIPI":                        {185, 200, 2},
	"CGCTransportadora":               {200, 214, 0},
	"TipoFrete":                       {214, 215, 0},
	"ValorFrete":                      {215, 230, 2},
	"PercentualDescontoComercial":     {230, 236, 4},
	"PercentualDescontoRepasseICMS":   {236, 242, 4},
	"TotalItens":                      {242, 255, 0},
	"TotalUnidades":                   {255, 266, 3},
	"TotalNotasParaPedido":            {266, 272, 0},
	"ValorDescontoICMSNormal":         {272, 287, 2},
	"ValorDescontoPIS":                {287, 302, 2},
	"ValorDescontoCofins":             {302, 317, 2},
	"TipoNotaFiscalEntrada":           {317, 319, 0},
	"ChaveAcessoNotaFiscalEletronica": {319, 363, 0},
}
